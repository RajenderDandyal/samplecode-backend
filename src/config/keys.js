require('dotenv').config();

module.exports = {
  DATABASE: process.env.DATABASE,
  JWT_SECRET_KEY: process.env.JWT_SECRET_KEY,
};
